#!/bin/bash

set -e

source $(pwd)/github-config

if [ -z "${GITHUB_ACCESS_TOKEN}" ]; then
    echo "Error 'GITHUB_ACCESS_TOKEN' not set. Check your github-config file."
    exit 1
fi
if [ -z "${GITHUB_ACCOUNT}" ]; then
    echo "Error 'GITHUB_ACCOUNT' not set. Check your github-config file."
    exit 1
fi

mkdir -p newman-output

echo "Running Postman/Newman to reset environment for the next demo..."
docker run \
  -v $(pwd)/src/setup/collections:/etc/newman \
  -v $(pwd)/newman-output:/newman-output \
  --network="iq-for-github-demo_default" \
  -t postman/newman:alpine run GitHub-Universe-Demo.postman_collection.json \
  --folder "Restart demo from scratch" \
  --env-var="IQ_HOST=iq-server" \
  --env-var="GITHUB_ACCESS_TOKEN=${GITHUB_ACCESS_TOKEN}" \
  --env-var="GITHUB_ACCOUNT=${GITHUB_ACCOUNT}" \
  --reporters cli,json \
  --reporter-json-export /newman-output

echo "Resetting the GitHub test repository for the next demo..."
git reset --hard $(cat $(pwd)/git-startup-commit)
git push origin HEAD -f