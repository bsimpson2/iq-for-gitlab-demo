#!/bin/bash

set -e

if [ -z "$(docker-compose ps -q github-actions-runnner)" ]
then
    echo "No GitHub Runner available, you may need a new GitHub Runner token..."
    exit 1
fi

source $(pwd)/github-config

mkdir -p newman-output

echo "Running Postman/Newman to perform one-time IQ setup... "
docker run \
  -v $(pwd)/src/setup/collections:/etc/newman \
  -v $(pwd)/newman-output:/newman-output \
  --network="iq-for-github-demo_default" \
  -t postman/newman:alpine run GitHub-Universe-Demo.postman_collection.json \
  --folder "Initial IQ Setup" \
  --env-var="IQ_HOST=iq-server" \
  --env-var="GITHUB_ACCESS_TOKEN=${GITHUB_ACCESS_TOKEN}" \
  --env-var="GITHUB_ACCOUNT=${GITHUB_ACCOUNT}" \
  --reporters cli,json \
  --reporter-json-export /newman-output

echo "IQ is now setup and the system is ready!"

echo "Enter your Sonatype email to receive NVS emails"
read SONATYPE_EMAIL

sed -e "s/\${iqServerUrl}/$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker-compose ps -q iq-server))/g" -e "s/\${sonatype-email}/${SONATYPE_EMAIL}/g" src/setup/nexus-iq.yml.template > .github/workflows/nexus-iq.yml
echo "Updated .github/workflows/nexus-iq.yml with IP address of the IQ server and Sonatype email. Committing this change to initiate a GitHub Actions CI build."

git add .github/workflows/nexus-iq.yml
git commit -m 'Configure IQ Server for IQ for GitHub Actions'

echo $(git rev-parse HEAD) > git-startup-commit

echo "Running the 'restart-demo.sh' script to setup the environment for the first demo!"
$(pwd)/src/setup/restart-demo.sh

if [ $? -eq 0 ]
then
  echo
  echo "=== One Time Initial Setup Part 2 is Complete ==="
  echo
  echo "You should have (in a minute or two)"
  echo " * A completed GitHub action CI build"
  echo " * A 'Demo App' in IQ with a clean report"
  echo
  echo "Ready to start a demo!"
  echo "Push a commit to uncomment 'jackson-databind' from the pom.xml"
  echo "After the demo run 'src/setup/restart-demo.sh' to reset"
else
  echo "Failure to run part 2 setup"
fi
